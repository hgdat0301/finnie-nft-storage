export const DEFAULT_CLIENT_OPTS = {
  serviceDID: 'did:key:z6MkrZ1r5XBFZjBU34qyD8fueMbMRkKw17BZaq2ivKFjnz2z',
  serviceURL: 'https://8609r1772a.execute-api.us-east-1.amazonaws.com',
  accessDID: 'did:key:z6MkkHafoFWxxWVNpNXocFdU6PL2RVLyTEgS1qTnD3bRP7V9',
  accessURL: 'https://access-api.web3.storage',
  settings: new Map()
}