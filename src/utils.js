import Client, { createClient } from '@web3-storage/w3up-client/src/index.js'
import { DEFAULT_CLIENT_OPTS } from './constant.js'

import fs from 'fs'
import { packToFs } from 'ipfs-car/pack/fs'
import { FsBlockStore } from 'ipfs-car/blockstore/fs'
import { Delegation, UCAN } from '@ucanto/core'

/*******************************************************************************/

/**
 * Register new client with email
 * @param {string} emailAddress Email address to register
 * @param {string} settingsPath Filepath to save the client setting after registration process
 * @returns {Client} new client created from emailAddress
 */
export async function registerNewClient(
  emailAddress,
  settingsPath = `${process.cwd()}/config/settings.json`
) {
  const client = createClient(DEFAULT_CLIENT_OPTS)

  try {
    console.log('Registering new client, check your email for verification...')
    const successMessage = await client.register(emailAddress)

    exportSetting(settingsPath, client.settings)

    console.log('Success: ', successMessage)
  } catch (err) {
    console.error('Registration failed: ', err)
  } finally {
    return client
  }
}

/**
 * Upload car file to IPFS
 * @param {Client} client the client will upload the file
 * @param {string} carFilePath Filepath to the upload .car file
 */
export async function uploadCAR(client, carFilePath) {
  const carData = await fs.promises.readFile(carFilePath)

  try {
    console.log('Uploading the data to storage...')
    const successMessage = await client.upload(carData)
    console.log(successMessage)
  } catch (err) {
    console.error(`upload error: ${err}`)
  }
}

/**
 * Generate car file and store in outFilePath
 * @param {string} inFilePath the location of source file
 * @param {string} outFilePath the location of generated car file
 */
export async function convertToCarFile(inFilePath, outFilePath) {
  await packToFs({
    input: inFilePath,
    output: outFilePath,
    blockstore: new FsBlockStore()
  })
}

/**
 * List all the uploaded files of the client
 * @param {Client} client
 */
export async function listUploads(client) {
  const cids = await client.list()

  console.log('cids', cids)
}

/**
 * Export the setting to JSON file
 * @param {string} outpath Filepath save location
 * @param {Map} setting Client settings Map
 */
export async function exportSetting(outpath, setting) {
  // Convert Map setting to JSON setting
  setting = Object.fromEntries(setting)

  /* CONVERT SETTING TO SAVE */
  if (setting.secret) {
    setting.secret = Buffer.from(setting.secret).toString('base64')
  }

  if (setting.agent_secret) {
    setting.agent_secret = Buffer.from(setting.agent_secret).toString('base64')
  }

  if (setting.account_secret) {
    setting.account_secret = Buffer.from(setting.account_secret).toString(
      'base64'
    )
  }

  if (setting.delegations) {
    for (const [did, del] of Object.entries(setting.delegations)) {
      const imported = Delegation.import([del?.ucan?.root])
      setting.delegations[did] = {
        ucan: UCAN.format(imported),
        alias: del.alias
      }
    }
  }

  /* WRITE SETTING TO FILE */
  const settingsJson = JSON.stringify(setting, null, 2)

  if (outpath) {
    fs.writeFileSync(outpath, settingsJson)
    console.log('Settings is written to:' + outpath)
  }
}

/**
 * Decode settings from jsonFile
 * @param {JSON} jsonFile
 * @returns client options to support createClient function
 */
export async function importSetting(jsonFile) {
  const CLIENT_OPTS = {
    serviceDID: 'did:key:z6MkrZ1r5XBFZjBU34qyD8fueMbMRkKw17BZaq2ivKFjnz2z',
    serviceURL: 'https://8609r1772a.execute-api.us-east-1.amazonaws.com',
    accessDID: 'did:key:z6MkkHafoFWxxWVNpNXocFdU6PL2RVLyTEgS1qTnD3bRP7V9',
    accessURL: 'https://access-api.web3.storage',
    settings: new Map()
  }

  function importBuffer(key) {
    const parsed = Uint8Array.from(Buffer.from(jsonFile[key], 'base64'))

    if (parsed) {
      CLIENT_OPTS.settings.set(key, parsed)
    }
  }

  for (var key of Object.keys(jsonFile)) {
    if (
      key === 'secret' ||
      key === 'agent_secret' ||
      key === 'account_secret'
    ) {
      importBuffer(key)
    } else if (key === 'delegations') {
      const delegations = {}

      for (const [did, del] of Object.entries(jsonFile.delegations)) {
        const ucan = UCAN.parse(del?.ucan)
        const root = await UCAN.write(ucan)
        delegations[did] = {
          ucan: Delegation.create({ root }),
          alias: del.alias
        }
      }

      CLIENT_OPTS.settings.set('delegations', delegations)
    } else {
      CLIENT_OPTS.settings.set(key, jsonFile[key])
    }
  }

  return CLIENT_OPTS
}
