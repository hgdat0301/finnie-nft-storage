import { createClient } from '@web3-storage/w3up-client/src/index.js'
import {
  registerNewClient,
  convertToCarFile,
  uploadCAR,
  listUploads,
  importSetting,
} from './utils.js'

import sample_settings from './config/sample_settings.json' assert { type: 'json' }
import settings from './config/settings.json' assert { type: 'json' }

/*******************************************************************************/
const testScript = async () => {
  // const client = await registerNewClient('dat.huynh@eastagile.com')

  const CLIENT_OPTS = await importSetting(sample_settings)

  const client = createClient(CLIENT_OPTS)

  // console.log(await client.whoami())
  // await convertToCarFile(
  //   `${process.cwd()}/assets/nft_collage.webp`,
  //   `${process.cwd()}/assets/nft_collage.car`
  // )

  // await uploadCAR(client, `${process.cwd()}/assets/nft_collage.car`)

  await listUploads(client)
}

testScript()
